const { conDb1 } = require('./items_requests');
const { itemsPerPage } = require('../defaults');

const itemsAll = (page, callback) => {

    let requestedQuery = 'SELECT * FROM items';

    if (page !== 0) {
        const itemsFromtId = (page * itemsPerPage) - itemsPerPage;
        requestedQuery = `SELECT * FROM items WHERE id>${itemsFromtId} ORDER BY id LIMIT ${itemsPerPage}`;
    }

    conDb1(requestedQuery, result => {
        if (result.fatal) {
            callback({
                error: result.code
            });
            return console.log('ERROR:', result.code);
        }
        const data = JSON.parse(JSON.stringify(result));
        callback(data);
    });

};

module.exports = itemsAll;
