const { conDb1 } = require('./items_requests');
const { itemsPerPage } = require('../defaults');

const pagesCount = (empty, callback) => {

    const requestedQuery = 'SELECT COUNT(id) FROM items;';

    conDb1(requestedQuery, result => {
        if (result.fatal) {
            callback({
                error: result.code
            });
            return console.log('ERROR:', result.code);
        }
        const records = Object.values(JSON.parse(JSON.stringify(result[0])) )[0];
        const pagesQtt = Math.ceil(records / itemsPerPage);
        callback(pagesQtt);
    });

};

module.exports = pagesCount;
