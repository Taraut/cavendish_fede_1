const { conDb1 } = require('./items_requests');
const { itemsPerPage } = require('../defaults');

const itemsAll = (page, callback) => {
    let requestedQuery = 'SELECT id, name, surname, email FROM `items` ORDER BY surname ASC';

    if (page !== 0) {
        const offset = page * itemsPerPage - itemsPerPage;
        requestedQuery = `SELECT id, name, surname, email FROM items ORDER BY surname ASC LIMIT ${offset},${itemsPerPage}`;
    }

    conDb1(requestedQuery, (result) => {
        if (result.fatal) {
            callback({
                error: result.code,
            });
            return console.log('ERROR:', result.code);
        }
        const data = JSON.parse(JSON.stringify(result));
        callback(data);
    });
};

module.exports = itemsAll;
