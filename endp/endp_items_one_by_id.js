const itemsOneById = require('../db_requests/items_one_by_id.js');

const endpItemsOneById = (req, res) => {

    if (!Number(req.query.id)) {
        res.send({
            error: 'No Item Id provided'
        });
        return;
    }

    const id = Number(req.query.id);

    itemsOneById(id, response => {
        if (response.error) {
            res.send({
                item: {},
                error: response.error
            });
            return;
        }
        res.send({
            item: response
        });
    });
    return;
};
module.exports = endpItemsOneById;
