import React from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import { Row, Col } from 'antd';
import Header from './components/header';
import HeaderMenu from './components/headerMenu';
import Section1 from './components/section1/section1';
import Section2 from './components/section2/section2';
import Section3 from './components/section3/section3';
import Section4 from './components/section4/section4';

const AppRouter = () => (
    <BrowserRouter>
        <Row>
            <Header />
            <HeaderMenu />
            <Col span={2} />
            <Col span={20} className="mainPart">
                <nav>
                    <Switch>
                        <Route exact path="/" component={Section1} />
                        <Route exact path="/section1" component={Section1} />
                        <Route exact path="/section2" component={Section2} />
                        <Route exact path="/section3" component={Section3} />
                        <Route exact path="/section4" component={Section4} />
                    </Switch>
                </nav>
            </Col>
            <Col span={2} />
        </Row>
    </BrowserRouter>
);

export default AppRouter;
