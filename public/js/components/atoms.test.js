import React from 'react';
import TestRenderer from 'react-test-renderer';
import { MyButton } from './atoms';

const data = {iconType: 'left'};
const onClick = () => {};
const testRenderer = TestRenderer.create(
    <MyButton data={data} text="Test Text" onClick={onClick} />
);
const testInstance = testRenderer.root;

test('MyButton has text', () => {
    expect(testInstance.findByProps({ className: 'MyButton_Text' }).children).toEqual([
        'Test Text'
    ]);
});
