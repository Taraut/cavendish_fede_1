import React from 'react';
import { Link } from 'react-router';
import { Menu } from 'antd';

class HeaderMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentMenuItem: 'section1',
        };
    }

    render() {
        const { currentMenuItem } = this.state;
        return (
            <div className="headerMenuWrapper">
                <Menu selectedKeys={[currentMenuItem]} mode="horizontal">
                    <Menu.Item key="s1" onClick={this.handleClickS1}>
                        <a href="./section1">Section 1: Arrays</a>
                    </Menu.Item>
                    <Menu.Item key="s2" onClick={this.handleClickS2}>
                        <a href="./section2">Section 2: Promises</a>
                    </Menu.Item>
                    <Menu.Item key="3" onClick={this.handleClickS2}>
                        <a href="./section3">Section 3: Discussion</a>
                    </Menu.Item>
                    <Menu.Item key="s4" onClick={this.handleClickS2}>
                        <a href="./section4">Section 4: Vodafone Exercise</a>
                    </Menu.Item>
                </Menu>
            </div>
        );
    }
}

export default HeaderMenu;
