export const MapArrayXmfactorCalc = (s1array, mfactor) => {
    return s1array.map((item, i) => item * mfactor);
};

export const FilterForEvenNumbersArrayCalc = (s1array) => {
    return s1array.filter((number) => number % 2 === 0);
};
