import { MapArrayXmfactorCalc, FilterForEvenNumbersArrayCalc } from './s1_calc';

const s1array = [1, 2, 3, 4];
const mfactor = 2;
const s1arrayExpected = [2, 4, 6, 8];

test('MapArrayXmfactorCalc returs X2  ', () => {
    expect(MapArrayXmfactorCalc(s1array, mfactor)).toMatchObject(s1arrayExpected);
});

const s1arrayTest = [1, 2, 3, 4];
const s1arrayExpectedEven = [2, 4];

test('FilterForEvenNumbersArrayCalc returs even numbers  ', () => {
    expect(FilterForEvenNumbersArrayCalc(s1arrayTest)).toMatchObject(s1arrayExpectedEven);
});
