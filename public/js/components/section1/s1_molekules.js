import React from 'react';
import PropTypes from 'prop-types';
import { MapArrayXmfactorCalc, FilterForEvenNumbersArrayCalc } from './s1_calc';

const PrintAllForeach = ({ s1array }) => {
    let myArrMadeFromForEach = [];
    s1array.forEach((item, i) =>
        myArrMadeFromForEach.push(<li key={i.toString() + 'q1'}>{item}</li>)
    );
    return <span className="textSimple">{myArrMadeFromForEach}</span>;
};

const MapArrayXmfactor = ({ s1array, mfactor }) => {
    const myArrCreatedFromMap = MapArrayXmfactorCalc(s1array, mfactor);

    return (
        <span className="textSimple">
            <PrintAllForeach s1array={myArrCreatedFromMap} />{' '}
        </span>
    );
};

const FilterForEvenNumbersArray = ({ s1array }) => {
    const evenNumbers = FilterForEvenNumbersArrayCalc(s1array);

    return (
        <span className="textSimple">
            <MapArrayXmfactor s1array={evenNumbers} mfactor={1} />
        </span>
    );
};

PrintAllForeach.propTypes = {
    s1array: PropTypes.array,
};
MapArrayXmfactor.propTypes = {
    s1array: PropTypes.array,
    mfactor: PropTypes.number,
};
FilterForEvenNumbersArray.propTypes = {
    s1array: PropTypes.array,
};

PrintAllForeach.defaultProps = {
    s1array: [1, 2, 3, 4],
};
MapArrayXmfactor.defaultProps = {
    s1array: [1, 2, 3, 4],
    mfactor: 2,
};
FilterForEvenNumbersArray.defaultProps = {
    s1array: [1, 2, 3, 4],
};

export { PrintAllForeach, MapArrayXmfactor, FilterForEvenNumbersArray };
