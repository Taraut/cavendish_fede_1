import React from 'react';
import TestRenderer from 'react-test-renderer';
import { PrintAllForeach, MapArrayXmfactor, FilterForEvenNumbersArray } from './s1_molekules';

const testRendererPrintAllForeach = TestRenderer.create(<PrintAllForeach />);
const testInstancePrintAllForeach = testRendererPrintAllForeach.root;

test('PrintAllForeach has li element', () => {
    expect(testInstancePrintAllForeach.findByProps({ className: 'textSimple' })).toBeDefined();
});

const testRendererMapArrayXmfactor = TestRenderer.create(<MapArrayXmfactor />);
const testInstanceMapArrayXmfactor = testRendererMapArrayXmfactor.root;

test('PrintAllForeach has li element', () => {
    expect(testInstanceMapArrayXmfactor.findByProps({ className: 'textSimple' })).toBeDefined();
});

const testRendererFilterForEvenNumbersArray = TestRenderer.create(<FilterForEvenNumbersArray />);
const testInstanceFilterForEvenNumbersArray = testRendererFilterForEvenNumbersArray.root;

test('PrintAllForeach has li element', () => {
    expect(testInstanceFilterForEvenNumbersArray.findByProps({ className: 'textSimple' })).toBeDefined();
});
