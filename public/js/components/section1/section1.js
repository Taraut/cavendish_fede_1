import React from 'react';
import { Row, Col } from 'antd';
import { TextTitle, TextSmall } from '../atoms';
import { PrintAllForeach, MapArrayXmfactor, FilterForEvenNumbersArray } from './s1_molekules';

class Section1 extends React.Component {

    render() {

        let arr = [1,2,3,4];

        return (
            <div>
                <Row>
                    <Col>Section 1: Arrays</Col>
                </Row>
                <Row>
                    <Col>public/js/components/section1</Col>
                </Row>
                <Row>
                    <Col>
                        <TextTitle text="Question 1:" />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <TextSmall text="Using foreach, print the elements of arr" />
                        <PrintAllForeach s1array={arr} />
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <TextTitle text="Question 2:" />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <TextSmall text="Using map, change arr to be [2,4,6,8]" />
                        <MapArrayXmfactor s1array={arr}/>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <TextTitle text="Question 3:" />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <TextSmall text="Using filter, return even numbers in arr" />
                        <FilterForEvenNumbersArray s1array={arr} />
                    </Col>
                </Row>

            </div>
        );
    }
}

export default Section1;
