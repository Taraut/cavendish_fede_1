import React from 'react';
import ReactDOM from 'react-dom';
import Section1 from './section1';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Section1 />, div);
  ReactDOM.unmountComponentAtNode(div);
});

