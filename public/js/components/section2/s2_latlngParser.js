const LatlngParser = (latlng) => {
    const latlngSliced = latlng.split(',');

    const coordinates = {
        lat: latlngSliced[0].trim(),
        lng: latlngSliced[1].trim(),
    };

    return coordinates;
};

export default LatlngParser;
