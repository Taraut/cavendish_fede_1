import LatlngParser from './s2_latlngParser';

const latlng = '  51.5074,   0.1278    ';
const coordinates = {
    lat: '51.5074',
    lng: '0.1278',
};

test('LatlngParser returs COORDINATES: lat, lng  ', () => {
    expect(LatlngParser(latlng)).toMatchObject(coordinates);
});
