import React from 'react';
import { Row, Col, Input } from 'antd';
import axios from 'axios';
import { TextTitle, TextSmall } from '../atoms';
import LatlngParser from './s2_latlngParser';
const { openCagedata, openweathermap } = require('defaults');

const { Search } = Input;

class Section2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cityByCoordinates: 'N/A',
            cityCurrency: 'N/A',
            latlngWeather: {
                description: 'N/A',
                temp: 'N/A',
                humidity: 'N/A',
                speed: 'N/A',
            },
            cityWeather: {
                description: 'N/A',
                temp: 'N/A',
                humidity: 'N/A',
                speed: 'N/A',
            },
        };

        this.cityFinder = this.cityFinder.bind(this);
        this.weatherCurrencyByCity = this.weatherCurrencyByCity.bind(this);
    }

    opencagedataApi(apiUrl) {
        return Promise.resolve(
            axios
                .get(apiUrl)
                .then((res) => {
                    const apiResponse = {
                        city: JSON.stringify(res.data.results[0].components.city),
                        currency: JSON.stringify(res.data.results[0].annotations.currency.name),
                        latlng: JSON.stringify(res.data.results[0].geometry),
                    };
                    return apiResponse;
                })
                .catch((error) => {
                    const apiResponse = {
                        error: error,
                        city: 'ERROR',
                        currency: 'ERROR',
                        latlng: 'ERROR',
                    };
                    return apiResponse;
                })
        );
    }

    openweathermapApi(apiUrlw) {
        return Promise.resolve(
            axios
                .get(apiUrlw)
                .then((res) => {
                    const weather = {
                        description: res.data.weather[0].description,
                        temp: res.data.main.temp,
                        humidity: res.data.main.humidity,
                        speed: res.data.wind.speed,
                    };
                    return weather;
                })
                .catch((error) => {
                    console.log(error);
                    const weather = {
                        error: error,
                        description: 'ERROR',
                        temp: 'ERROR',
                        humidity: 'ERROR',
                        speed: 'ERROR',
                    };
                    return weather;
                })
        );
    }

    cityFinder(latlng) {
        const coordinates = LatlngParser(latlng);

        // for test: https://api.opencagedata.com/geocode/v1/json?q=51.5074, 0.1278&key=bb07648fdd5c4f54b3ae0d3f7fb0d5b9
        const apiUrl = `${openCagedata.urlBase}json?q=${coordinates.lat}, ${coordinates.lng}&key=${openCagedata.apiKey}`;

        this.opencagedataApi(apiUrl).then((result) => {
            this.setState({
                cityByCoordinates: result.city,
            });

            // for test: http://api.openweathermap.org/data/2.5/weather?lat=51.5074&lon=0.1278&units=metric&appid=7eb65d7569d112667fd03d735ec2867b
            const apiUrlw = `${openweathermap.urlBase}?lat=${coordinates.lat}&lon=${coordinates.lng}&units=metric&appid=${openweathermap.apiKey}`;

            this.openweathermapApi(apiUrlw).then((weatherResult) => {
                this.setState({
                    latlngWeather: {
                        description: weatherResult.description,
                        temp: weatherResult.temp,
                        humidity: weatherResult.humidity,
                        speed: weatherResult.speed,
                    },
                });
            });
        });
    }

    weatherCurrencyByCity(cityWc) {
        const apiUrl = `${openCagedata.urlBase}json?q=${cityWc}&key=${openCagedata.apiKey}`;
        let latlng = {};
        this.opencagedataApi(apiUrl).then((result) => {
            this.setState({
                cityCurrency: result.currency,
            });

            const coordinates = result.latlng;
            // for test: http://api.openweathermap.org/data/2.5/weather?lat=51.5074&lon=0.1278&units=metric&appid=7eb65d7569d112667fd03d735ec2867b
            const apiUrlw = `${openweathermap.urlBase}?q=${cityWc}&units=metric&appid=${openweathermap.apiKey}`;
            this.openweathermapApi(apiUrlw).then((weatherResult) => {
                this.setState({
                    cityWeather: {
                        description: weatherResult.description,
                        temp: weatherResult.temp,
                        humidity: weatherResult.humidity,
                        speed: weatherResult.speed,
                    },
                });
            });
        });
    }

    render() {
        const { cityByCoordinates, latlngWeather, cityCurrency, cityWeather } = this.state;

        const latlngWeatherTxt = `Weather for the given lat/long: ${latlngWeather.description},
        temp: ${latlngWeather.temp} C,
        humidity: ${latlngWeather.humidity} %,
        wind: ${latlngWeather.speed} m/s`;

        const cityWeatherTxt = `Weather for the given lat/long: ${cityWeather.description},
        temp: ${cityWeather.temp} C,
        humidity: ${cityWeather.humidity} %,
        wind: ${cityWeather.speed} m/s`;

        return (
            <div>
                <Row>
                    <Col>Section 2: Promises</Col>
                </Row>
                <Row>
                    <Col>public/js/components/section2</Col>
                </Row>
                <Row>
                    <Col>
                        <TextTitle text="Question 1:" />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <TextSmall text="Print the city for lat/long" />

                        <Search
                            placeholder="lat, lng"
                            onSearch={(value) => this.cityFinder(value)}
                            style={{ width: 200 }}
                            defaultValue="51.5074, 0.1278"
                        />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <span>Sity is {cityByCoordinates}</span>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <TextTitle text="Question 2:" />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <TextSmall text="Print the weather for a given long / lan" />
                    </Col>
                </Row>
                <Row>
                    <Col>{latlngWeatherTxt}</Col>
                </Row>

                <Row>
                    <Col>
                        <TextTitle text="Question 3:" />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <TextSmall text="Print the weather and currency for a given city" />
                        <Search
                            placeholder="City"
                            onSearch={(value) => this.weatherCurrencyByCity(value)}
                            style={{ width: 200 }}
                            defaultValue="London"
                        />
                    </Col>
                </Row>
                <Row>
                    <Col>Currency: {cityCurrency}</Col>
                </Row>
                <Row>
                    <Col>Weather: {cityWeatherTxt}</Col>
                </Row>
            </div>
        );
    }
}

export default Section2;
