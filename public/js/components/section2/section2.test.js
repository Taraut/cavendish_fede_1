import React from 'react';
import ReactDOM from 'react-dom';
import Section2 from './section2';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Section2 />, div);
    ReactDOM.unmountComponentAtNode(div);
  });


