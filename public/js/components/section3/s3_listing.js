import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { Row, Col, Menu, Alert, Pagination, Modal } from 'antd';
import ProfilePage from './s3_profile_page';

class Listing extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: false,
            currentPage: 1,
            itemsPagesCount: 0,
            showProfilePage: false,
            id: 1,
        };

        this.paginatorOnChange = this.paginatorOnChange.bind(this);
        this.menuOnClick = this.menuOnClick.bind(this);
        this.getItems = this.getItems.bind(this);
        this.handleOk = this.handleOk.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    componentDidMount() {
        const urlSearchData = this.props.urlSearchData;
        const page = Number.parseInt(urlSearchData.currentPage)
            ? Number.parseInt(urlSearchData.currentPage)
            : 1;
        this.paginatorOnChange(page);
    }

    getItems() {
        const { currentPage } = this.state;
        const apiUrl = `../listing?page=${currentPage}`;

        axios
            .get(apiUrl)
            .then((res) => {
                const { itemsPagesCount, items } = res.data;
                const error = res.data.error ? res.data.error : false;

                let titles = [];
                items.forEach((item) => {
                    titles.push(item.title);
                });

                this.setState({
                    itemsPagesCount: itemsPagesCount,
                    items: items,
                    titles: titles,
                    error: error,
                });
            })
            .catch((error) => {
                this.setState({
                    itemsPagesCount: 0,
                    items: [],
                    error: error.toString(),
                });
            });
    }

    paginatorOnChange(page) {
        this.setState(
            {
                currentPage: page,
            },
            () => this.getItems()
        );
    }

    menuOnClick({ key }) {
        this.setState({
            id: key,
            showProfilePage: true,
        });
    }

    handleOk() {
        this.setState({
            showProfilePage: false,
        });
    }
    handleCancel() {
        this.setState({
            showProfilePage: false,
        });
    }

    render() {
        const { items, error, itemsPagesCount, currentPage } = this.state;

        const id = this.state.id.toString();

        return (
            <Row>
                <Col>
                    {error && (
                        <Alert message="Error" description={error} type="error" showIcon closable />
                    )}

                    {items && (
                        <Row>
                            <Menu
                                style={{ width: '100%' }}
                                defaultSelectedKeys={['0']}
                                defaultOpenKeys={['sub1']}
                                mode="inline"
                                onClick={this.menuOnClick}
                            >
                                <Menu.ItemGroup key="g1" title="Sorted by surname">
                                    {items.map((item) => (
                                        <Menu.Item key={item.id} className="itemsTable_menuItem">
                                            <Row>
                                                <Col span={18} className="itemsTable_title">
                                                    {item.name} {item.surname}
                                                    <br />
                                                    <span className="itemsTable_text">
                                                        {item.email}
                                                    </span>
                                                </Col>
                                            </Row>
                                        </Menu.Item>
                                    ))}
                                </Menu.ItemGroup>
                            </Menu>
                            <Pagination
                                current={currentPage}
                                defaultPageSize={1}
                                total={itemsPagesCount}
                                onChange={this.paginatorOnChange}
                                hideOnSinglePage={true}
                            />

                            <Modal
                                title="Profile"
                                visible={this.state.showProfilePage}
                                onOk={this.handleOk}
                                onCancel={this.handleCancel}
                                destroyOnClose={true}
                            >
                                <ProfilePage id={id} />
                            </Modal>
                        </Row>
                    )}
                </Col>
            </Row>
        );
    }
}

export default Listing;

Listing.propTypes = {
    urlSearchData: PropTypes.object,
};
Listing.defaultProps = {
    urlSearchData: {},
};
