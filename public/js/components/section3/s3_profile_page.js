import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { Row, Col, Alert, Spin } from 'antd';
import { TextTitle, TextSimple } from '../atoms';

class ProfilePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: true,
            error: false,
        };
    }

    componentDidMount() {
        this.getOneItemById();
    }

    getOneItemById() {
        const apiUrl = `../itemsonebyid?id=${this.props.id}`;
        this.setState({
            isLoaded: false,
        });

        axios
            .get(apiUrl)
            .then((res) => {
                const { item } = res.data;
                const error = res.data.error ? res.data.error : false;

                this.setState({
                    item: item[0],
                    error: error,
                    isLoaded: true,
                });
            })
            .catch((error) => {
                this.setState({
                    item: [],
                    error: error.toString(),
                    isLoaded: true,
                });
            });
    }

    render() {
        const { error, isLoaded, item } = this.state;

        const name = item ? item.name : false;
        const surname = item ? item.surname : false;
        const email = item ? item.email : false;
        const title = item ? item.title : false;
        const text = item ? item.text : false;

        return (
            <Row>
                <Col>
                    {error && (
                        <Alert
                            message="Error"
                            description={error}
                            type="error"
                            showIcon
                            closable
                            onClose={this.backToItemsTable}
                        />
                    )}

                    {!isLoaded && <Spin size="large"></Spin>}

                    {name && <TextTitle text={name} />}
                    {surname && <TextTitle text={surname} />}
                    {email && <TextSimple text={email} />}
                    {title && <TextSimple text={title} />}
                    {text && <TextSimple text={text} />}
                </Col>
            </Row>
        );
    }
}

export default ProfilePage;

ProfilePage.propTypes = {
    id: PropTypes.string,
};
ProfilePage.defaultProps = {
    id: '1',
};
