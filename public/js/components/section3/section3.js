import React from 'react';
import { Row, Col } from 'antd';
import { TextTitle, TextSimple } from '../atoms';
import parsingURLSearchParams from '../parsingURLSearchParams';
import Listing from './s3_listing';

const Section3 = (props) => {
    const urlSearchData = parsingURLSearchParams(props.location.search);

    return (
        <div>
            <Row>
                <Col>Section 3: Discussion</Col>
            </Row>
            <Row>
                <Col>
                    <TextTitle text="Profile Listing Page" />
                </Col>
            </Row>
            <Row>
                <Listing urlSearchData={urlSearchData} />
            </Row>
        </div>
    );
};

export default Section3;
