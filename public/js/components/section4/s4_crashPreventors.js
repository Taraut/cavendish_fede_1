const crashPreventors = (data) => {
    // check for data is array. Not in spec, but just for crash prevention.
    if (!Array.isArray(data)) {
        return { title: 'DATA ERROR', message: 'Data not an array' };
    }

    const dataLenght = data.length;

    // No Empty data array. Not in spec, but just for crash prevention.
    if (dataLenght === 0) {
        return { title: 'DATA ERROR', message: 'Empty data array' };
    }

    for (let i = 0; i < dataLenght; i++) {
        // check for data array elemet is an object. Not in spec, but just for crash prevention.
        if (typeof data[i] !== 'object') {
            return { title: 'DATA ERROR', message: 'Array element not an object' };
        }

        // check for data array object has must have element 'state. Not in spec, but just for crash prevention.
        if (!data[i].hasOwnProperty('state')) {
            return { title: 'DATA ERROR', message: 'No "state" property in object' };
        }
    }

    // 2 or more objects with "state" not "processing". Not in spec, but just for crash prevention.
    let stateCheck = 0;
    for (let i = 0; i < dataLenght; i++) {
        if (data[i].state !== 'processing') {
            stateCheck++;
            if (stateCheck > 1) {
                return {
                    title: 'DATA ERROR',
                    message: '2 or more objects with "state" not "processing"',
                };
            }
        }
    }

    return;
};

export default crashPreventors;
