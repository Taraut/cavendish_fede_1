import crashPreventors from './s4_crashPreventors';

const data = 'XXXX';
const expectedReturn = {
    title: 'DATA ERROR', message: 'Data not an array'
};

test('Check for data is array', () => {
    expect(crashPreventors(data)).toMatchObject(expectedReturn);
});

const data1 = [];
const expectedReturn1 = {
    title: 'DATA ERROR', message: 'Empty data array'
};

test('Check for Empty data array', () => {
    expect(crashPreventors(data1)).toMatchObject(expectedReturn1);
});


const data2 = [12,12];
const expectedReturn2 = {
    title: 'DATA ERROR', message: 'Array element not an object'
};

test('check for data array elemet is an object.', () => {
    expect(crashPreventors(data2)).toMatchObject(expectedReturn2);
});

const data3 = [{qwe: 'qwe'}];
const expectedReturn3 = {
    title: 'DATA ERROR', message: 'No "state" property in object'
};

test('check for data array object has must have element "state"', () => {
    expect(crashPreventors(data3)).toMatchObject(expectedReturn3);
});

const data4 = [{ state: 'error', errorCode: 'INCORRECT_DETAILS' }, { state: 'error', errorCode: 'INCORRECT_DETAILS' }];
const expectedReturn4 = {
    title: 'DATA ERROR',
    message: '2 or more objects with "state" not "processing"',
};

test('2 or more objects with "state" not "processing"', () => {
    expect(crashPreventors(data4)).toMatchObject(expectedReturn4);
});
