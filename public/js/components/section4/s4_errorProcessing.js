const errorProcessing = (data) => {
    // check for data array object with status: "error" has must have element 'errorCode'. Not in spec, but just for crash prevention.
    if (!data.hasOwnProperty('errorCode')) {
        return { title: 'DATA ERROR', message: 'No "errorCode" property in error related object' };
    }

    if (data.errorCode === 'NO_STOCK') {
        return { title: 'Error page', message: 'No stock has been found' };
    }

    if (data.errorCode === 'INCORRECT_DETAILS') {
        return { title: 'Error page', message: 'Incorrect details have been entered' };
    }

    if (data.errorCode === null || data.errorCode ===undefined) {
        return { title: 'Error page', message: null };
    }

    // check for errorCode. Not in spec, but just for crash prevention.
    return { title: 'DATA ERROR', message: 'Unknown "errorCode" in error related object' };
};

export default errorProcessing;
