import errorProcessing from './s4_errorProcessing';

const data = { state: 'error' };
const expectedReturn = {
    title: 'DATA ERROR',
    message: 'No "errorCode" property in error related object',
};

test('Check for data array object with status: "error" has must have element "errorCode', () => {
    expect(errorProcessing(data)).toMatchObject(expectedReturn);
});

const data1 = { state: 'error', errorCode: 'NO_STOCK' };
const expectedReturn1 = {
    title: 'Error page',
    message: 'No stock has been found',
};

test('Check for errorCode === NO_STOCK', () => {
    expect(errorProcessing(data1)).toMatchObject(expectedReturn1);
});

const data2 = { state: 'error', errorCode: 'INCORRECT_DETAILS' };
const expectedReturn2 = {
    title: 'Error page',
    message: 'Incorrect details have been entered',
};

test('Check for errorCode === NO_STOCK', () => {
    expect(errorProcessing(data2)).toMatchObject(expectedReturn2);
});

const data3 = { state: 'error', errorCode: null };
const expectedReturn3 = {
    title: 'Error page',
    message: null,
};

test('Check for data.errorCode === null', () => {
    expect(errorProcessing(data3)).toMatchObject(expectedReturn3);
});

const data4 = { state: 'error', errorCode: undefined };
const expectedReturn4 = {
    title: 'Error page',
    message: null,
};

test('Check for data.errorCode === undefined', () => {
    expect(errorProcessing(data4)).toMatchObject(expectedReturn4);
});

const data5 = { state: 'error', errorCode: 'Wrong code' };
const expectedReturn5 = {
    title: 'DATA ERROR',
    message: 'Unknown "errorCode" in error related object',
};

test('Check for data.errorCode === undefined', () => {
    expect(errorProcessing(data5)).toMatchObject(expectedReturn5);
});
