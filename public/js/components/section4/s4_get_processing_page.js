import crashPreventors from './s4_crashPreventors';
import stateProcessing from './s4_stateProcessing';

const getProcessingPage = (data) => {
    const dataLenght = data.length;

    const crashPreventorsResult = crashPreventors(data);
    if (crashPreventorsResult) {
        return crashPreventorsResult;
    }

    if (data[0].state !== 'processing') {
        return stateProcessing(data[0]);
    }

    if (data[0].state === 'processing') {
        // After 'state: "processing" must be next element. Not in spec, but just for crash prevention.
        if (dataLenght === 1) {
            return {
                title: 'DATA ERROR',
                message: 'state: "processing" in array only',
            };
        }
        const processingsQtt = dataLenght - 1;

        setTimeout(() => {
            const result = stateProcessing(data[processingsQtt]);
            console.log(result);
            return result;
        }, 2000 * processingsQtt);
    }
};

export default getProcessingPage;
