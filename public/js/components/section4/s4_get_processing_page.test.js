import getProcessingPage from './s4_get_processing_page';

const data = 'XXXX';
const expectedReturn = {
    title: 'DATA ERROR',
    message: 'Data not an array',
};

test('Check for crashPreventorsResult request', () => {
    expect(getProcessingPage(data)).toMatchObject(expectedReturn);
});

const data3 = [{ state: 'error', errorCode: 'INCORRECT_DETAILS' }];
const expectedReturn3 = {
    title: 'Error page',
    message: 'Incorrect details have been entered',
};

test('Check for state: "error" in getProcessingPage', () => {
    expect(getProcessingPage(data3)).toMatchObject(expectedReturn3);
});

const data4 = [{ state: 'processing' }];
const expectedReturn4 = {
    title: 'DATA ERROR',
    message: 'state: "processing" in array only',
};

test('Check for state: "processing" in array only in getProcessingPage', () => {
    expect(getProcessingPage(data4)).toMatchObject(expectedReturn4);
});

const data5 = [
    { state: 'processing' },
    { state: 'processing' },
    { state: 'error', errorCode: 'INCORRECT_DETAILS' },
];
const expectedReturn5 = {
    title: 'Error page',
    message: 'Incorrect details have been entered',
};

test('Check for 2x"processing" and "error" in getProcessingPage', () => {
    const test = getProcessingPage(data5);

    setTimeout(() => {
        test.toMatchObject(expectedReturn5);
    }, 4000);
});
