import errorProcessing from './s4_errorProcessing';

const stateProcessing = (data) => {
    if (data.state === 'success') {
        return { title: 'Order complete', message: null };
    }

    if (data.state === 'error') {
        return errorProcessing(data);
    }

    // After this point no more known statuses, 'processing' not processing here. Not in spec, but just for crash prevention.
    return { title: 'DATA ERROR', message: 'Unknown "state" property' };
};

export default stateProcessing;
