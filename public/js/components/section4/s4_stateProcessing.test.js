import stateProcessing from './s4_stateProcessing';

const data1 = { state: 'success' };
const expectedReturn1 = {
    title: 'Order complete',
    message: null,
};
console.log(data1.state);
test('Check for "success"', () => {
    expect(stateProcessing(data1)).toMatchObject(expectedReturn1);
});

const data3 = { state: 'error', errorCode: 'INCORRECT_DETAILS' };
const expectedReturn3 = {
    title: 'Error page',
    message: 'Incorrect details have been entered',
};

test('Check for state: "error", errorCode: "INCORRECT_DETAILS"', () => {
    expect(stateProcessing(data3)).toMatchObject(expectedReturn3);
});

const data4 = { state: 'XXX' };
const expectedReturn4 = {
    title: 'DATA ERROR',
    message: 'Unknown "state" property',
};

test('Unknown "state" property', () => {
    expect(stateProcessing(data4)).toMatchObject(expectedReturn4);
});
