import React from 'react';
import { Row, Col } from 'antd';
import { TextSmall } from '../atoms';
import getProseccingPage from './s4_get_processing_page';

class Section4 extends React.Component {
    componentDidMount() {
        // const data = 'jhhjhj';
        // const data = [1,12];
        // const data = [{ ustate: 'processing' }];
        // const data = [{ state: 'XXXX' }, { state: 'error' }];
        // const data = [{ state: 'processing' }];
        // const data = [{ state: 'success' }];
        // const data = [{ state: 'error', errorCode: 'INCORRECT_DETAILS' }];
        // const data = [{ state: 'error', errorCode: undefined }];
        // const data = [{ state: 'error', errorCode: null }];
        // const data = [{ state: 'processing' },{ state: 'error', errorCode: 'INCORRECT_DETAILS' }];
        // const data = [{ state: 'error', errorCode: 'INCORRECT_DETAILS' }, { state: 'error', errorCode: 'INCORRECT_DETAILS' }];
        // const data = [{ state: 'processing' }, { state: 'error' }];
        // const data = [{ state: 'processing' }, { state: 'processing' }, { state: 'error' }];
        const data = [
            { state: 'processing' },
            { state: 'processing' },
            { state: 'error', errorCode: 'INCORRECT_DETAILS' },
        ];

        console.log(getProseccingPage(data));
    }

    render() {
        return (
            <div>
                <Row>
                    <Col>Section 4: Vodafone Exercise</Col>
                </Row>
                <Row>
                    <Col>
                        <TextSmall text="Please see: public/js/components/section4" />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <TextSmall text="section4.js ComponentDidMount() has different data examples.
                        Use custom command: npm run-script run, after changes and see console for output." />
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <TextSmall text="Easy way: run unit tests (npm test)" />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <TextSmall text="getProseccingPage(data) is located: public/js/components/section4/s4_get_processing_page.js" />
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Section4;
