// Action generators  // usage sample: store.dispatch(incrementCount({ incrementBy: 5 }));
// GET_ITEMS_TITLE

const getItemsTitle = ( {titles = 'No titles', itemUrl = '/'} = {}) => ({
    type: 'GET_ITEMS_TITLE',
    titles: titles,
    itemUrl: itemUrl
});

export { getItemsTitle }
