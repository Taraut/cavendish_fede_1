import { getItemsTitle } from './actions';

describe('actions', () => {
    it('should creat an action GET_ITEMS_TITLE', () => {
        const data = {
            titles: ['Item-1', 'Item-2'],
            itemUrl: './test_url'
        };
        const expectedAction = {
            type: 'GET_ITEMS_TITLE',
            titles: data.titles,
            itemUrl: data.itemUrl
        };
        expect(getItemsTitle(data)).toEqual(expectedAction);
    });
});

describe('actions', () => {
    it('should creat an action GET_ITEMS_TITLE with default parms', () => {
        const data1 = {};
        const expectedAction1 = {
            type: 'GET_ITEMS_TITLE',
            titles: 'No titles',
            itemUrl: '/'
        };
        expect(getItemsTitle(data1)).toEqual(expectedAction1);
    });
});
