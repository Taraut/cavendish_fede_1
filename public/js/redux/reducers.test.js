import { itemsReducer } from './reducers';

describe('reducers', () => {
    it('reducer default params', () => {
        expect(itemsReducer(undefined, {})).toEqual([
            {
                titles: ['No titles (reducer default)'],
                itemUrl: '/'
            }
        ]);
    });

    it('reducer default params for GET_ITEMS_TITLE', () => {
        expect(
            itemsReducer(undefined, {
                action: { type: 'GET_ITEMS_TITLE' }
            })
        ).toEqual([
            {
                titles: ['No titles (reducer default)'],
                itemUrl: '/'
            }
        ]);
    });

    it('should handle GET_ITEMS_TITLE', () => {
        expect(
            itemsReducer(
                [
                    {
                        titles: ['Title 1', 'Title 2'],
                        itemUrl: './testurl'
                    }
                ],
                {
                    action: {
                        type: 'GET_ITEMS_TITLE'
                    }
                }
            )
        ).toEqual([
            {
                titles: ['Title 1', 'Title 2'],
                itemUrl: './testurl'
            }
        ]);
    });
});
