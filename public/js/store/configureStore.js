import { createStore, combineReducers } from 'redux';
import { itemsReducer } from '../redux/reducers';

export default () => {
    const store = createStore(
        combineReducers({
        itemsReducer: itemsReducer
        })
    );
    return store;
};
